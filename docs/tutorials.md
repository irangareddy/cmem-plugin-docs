# Introduction

cmempy is a Python API wrapper around the eccenca Corporate Memory HTTP APIs which can be used to rapidly script processes which interact with Corporate Memory. cmempy is also the underlying Python module which powers the cmemc - Command Line Interface.

